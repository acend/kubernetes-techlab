---
title: "Labs"
weight: 2
menu:
  main:
    weight: 1
---

The goal of these techlabs is to create some hands-on tasks for people. By using the Kubernetes(k8s) platform you find an easy way to deploy and deliver your software packaged in container.

Goal of these techlabs:

- Having an easy start with this modern technology
- Understand the basic concepts
- Deploy applications on kubernetes


## Additional Docs

- [Kubernetes Docs](https://kubernetes.io/docs/home/?path=users&persona=app-developer&level=foundational)
- [Helm Docs](https://docs.helm.sh/)


## Additional Tutorials

- [Official Kubernetes Tutorial](https://kubernetes.io/docs/tutorials/)

