---
title: "10. Additional Concepts"
weight: 10
---

Kubernetes does not only know Pods, Deployments, Services etc, there are various other kinds or resources. In the next few labs we have a look at some of them.